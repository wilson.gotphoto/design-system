import { useMemo } from "react";
import tinycolor2 from "tinycolor2";

/**
 * Component to set theme color styles based on a custom primary color.
 * Put this into the head of your document
 *
 * @param primaryColor
 * @returns
 */
export const ThemeColorStyles = ({
  primaryColor,
}: {
  primaryColor?: string;
}) => {
  const hsl = useMemo(() => tinycolor2(primaryColor).toHsl(), [primaryColor]);

  const primaryLightness = Math.min(hsl.l, 0.7);
  const darkLightness = Math.min(Math.max(primaryLightness - 0.09, 0), 1);
  const lightLightness = Math.min(Math.max(primaryLightness + 0.09, 0), 1);
  const lighterLightness = Math.min(Math.max(primaryLightness + 0.2, 0), 1);

  const styles = `:root {
    --default-accent-color-h: ${hsl.h.toFixed(2)};
    --default-accent-color-s: ${(hsl.s * 100).toFixed(2)}%;
    --default-accent-color-l: ${(primaryLightness * 100).toFixed(2)}%;
    --default-legacy-dark-l: ${(darkLightness * 100).toFixed(2)}%;
    --default-legacy-light-l: ${(lightLightness * 100).toFixed(2)}%;
    --default-legacy-lighter-l: ${(lighterLightness * 100).toFixed(2)}%;
  }`;

  return <style>{styles}</style>;
};
