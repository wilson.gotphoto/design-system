import { XIcon } from "@heroicons/react/outline";
import clsx from "clsx";

export type CloseButtonProps = {
  onClick?: () => void;
  accessibleText?: string;
  ariaControls?: string;
  ariaExpanded?: boolean;
  className?: string;
};

export const CloseButton = ({
  onClick,
  accessibleText = "Close Panel",
  ariaControls = "panel",
  ariaExpanded,
  className,
}: CloseButtonProps) => {
  return (
    <button
      type="button"
      aria-controls={ariaControls}
      aria-expanded={ariaExpanded}
      className={clsx(
        "bg-white rounded-md text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2",
        className
      )}
      onClick={() => onClick?.()}
    >
      <span className="sr-only">{accessibleText}</span>
      <XIcon className="h-6 w-6" aria-hidden="true" />
    </button>
  );
};
