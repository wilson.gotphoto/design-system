import { render, fireEvent } from "@testing-library/react";
import { ComponentProps } from "react";

import { CloseButton, CloseButtonProps } from "./CloseButton";

type Props = ComponentProps<typeof CloseButton>;

const defaultProps = {
  onClick: jest.fn(),
  ariaExpanded: false,
} as CloseButtonProps;

const renderComponent = (props: Partial<Props> = {}) =>
  render(<CloseButton {...{ ...defaultProps, ...props }} />);

describe("<CloseButton />", () => {
  it("should render properly", () => {
    const { getByRole } = renderComponent();

    const component = getByRole("button");

    expect(component).toMatchSnapshot();
  });

  it("should call onClick when clicked", () => {
    const mockOnClick = jest.fn();
    const { getByRole } = renderComponent({ onClick: mockOnClick });

    const component = getByRole("button");

    fireEvent.click(component);

    expect(mockOnClick).toHaveBeenCalled();
  });

  it("should add a custom className", async () => {
    const testClassName = "test-className";
    const { getByRole } = renderComponent({ className: testClassName });

    const component = getByRole("button");

    expect(component).toHaveClass(testClassName);
  });
});
