export type NotFoundProps = { title: string; message: string };

export const NotFound = ({ title, message }: NotFoundProps) => {
  return (
    <div className="m-auto flex items-baseline justify-center divide-x divide-current">
      <h1 className="font-bold text-xl p-3">{title}</h1>
      <p className="p-3">{message}</p>
    </div>
  );
};
