import { render } from "@testing-library/react";
import { ComponentProps } from "react";

import { NotFound } from "./NotFound";

export const defaultProps = {
  title: "Oops",
  message: "This Page could not be found",
};

type Props = ComponentProps<typeof NotFound>;

const renderComponent = (props: Partial<Props> = {}) =>
  render(<NotFound {...{ ...defaultProps, ...props }} />);

describe("<NotFound />", () => {
  it("should render properly", () => {
    const { container } = renderComponent();

    expect(container).toMatchSnapshot();
  });
});
