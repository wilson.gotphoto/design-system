import clsx from "clsx";
import {
  ForwardedRef,
  forwardRef,
  MouseEventHandler,
  ReactNode,
  HTMLAttributeAnchorTarget,
} from "react";
import { Loading } from "../Icons";

// TODO: Check use cases 'plain' (-> no borders/bg) vs 'none' (->no style at all)
type ButtonType = "primary" | "highlight" | "outlined" | "plain" | "none";

export type CommonProps = {
  isLoading?: boolean;
  disabled?: boolean;
  children?: ReactNode;
  icon?: ReactNode;
  className?: string;
  type?: ButtonType;
};

type AsButtonProps = {
  as?: "button";
  onClick?: MouseEventHandler<HTMLButtonElement>;
};

type AsAnchorProps = {
  as: "a";
  onClick?: MouseEventHandler<HTMLAnchorElement>;
  href?: string;
  target?: HTMLAttributeAnchorTarget;
  rel?: string;
};

export type ButtonProps = CommonProps & (AsButtonProps | AsAnchorProps);

function getButtonClassName({
  type,
  isLoading,
  disabled,
  className,
}: {
  type: ButtonType;
  isLoading?: boolean;
  disabled?: boolean;
  className?: string;
}) {
  // TODO: disabled, focus & hover state styles
  let common =
    "px-4 py-2 rounded-lg inline-flex items-center font-medium transition-colors focus:outline-none focus:ring-2";
  let specific;

  switch (type) {
    case "outlined":
      specific = "border border-main-dark focus:ring-offset-brand-highlight";
      break;

    case "primary":
      specific =
        "text-white bg-brand-highlight focus:ring-offset-brand-highlight";
      break;

    case "highlight":
      specific = "text-black bg-brand-stroke focus:ring-offset-brand-stroke";
      break;

    case "plain":
      specific = "bg-transparent";
      break;

    default:
      common = "inline-flex";
      specific = "";
      break;
  }

  return clsx(
    common,
    specific,
    { "text-opacity-70 pointer-events-none": isLoading || disabled },
    className
  );
}

export const Button = forwardRef<
  HTMLAnchorElement | HTMLButtonElement,
  ButtonProps
>(function Button(props, ref) {
  const {
    type = "primary",
    icon,
    isLoading = false,
    disabled = false,
    children,
    className,
  } = props;

  const commonProps = {
    className: getButtonClassName({ type, isLoading, disabled, className }),
    disabled: isLoading || disabled,
    children: (
      <>
        {isLoading && (
          <Loading
            className={clsx(
              "animate-spin w-4 h-4 mr-2 opacity-100",
              type !== "none" && "-ml-1"
            )}
          />
        )}
        {!isLoading && icon && (
          <span className={clsx("mr-2", type !== "none" && "-ml-1")}>
            {icon}
          </span>
        )}
        {children}
      </>
    ),
  };

  // It is a <a> tag
  if (props.as === "a") {
    const { href, onClick, rel, target } = props;
    return (
      //  Disabled because children is inside commonProps
      // eslint-disable-next-line jsx-a11y/anchor-has-content
      <a
        ref={ref as ForwardedRef<HTMLAnchorElement>}
        {...commonProps}
        {...{ href, onClick, rel, target }}
      />
    );
  }

  // It is a <button>
  const { onClick } = props;
  return (
    <button
      ref={ref as ForwardedRef<HTMLButtonElement>}
      {...commonProps}
      {...{ onClick }}
    />
  );
});
