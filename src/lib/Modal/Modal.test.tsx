import { render } from "@testing-library/react";
import { ComponentProps } from "react";

import { TEST_IDS } from "../utils/test-ids";

import { Modal, ModalProps } from "./Modal";

const defaultProps = {
  children: <div>Modal Content Mock</div>,
  isOpen: true,
  onClose: jest.fn(),
} as ModalProps;

type Props = ComponentProps<typeof Modal>;

const renderComponent = (props: Partial<Props> = {}) =>
  render(<Modal {...{ ...defaultProps, ...props }} />);

describe("<Modal />", () => {
  it("should render properly", () => {
    const { getByTestId } = renderComponent();

    const component = getByTestId(TEST_IDS.LAYOUT_MODAL);

    expect(component).toMatchSnapshot();
  });

  it("should render properly when hidden", () => {
    const { queryByTestId } = renderComponent({ isOpen: false });

    const component = queryByTestId(TEST_IDS.LAYOUT_MODAL);

    expect(component).not.toBeInTheDocument();
  });
});
