declare module "csstype" {
  interface Properties {
    /** custom properties */
    [key: `--${string}`]: string | number;
  }
}
