module.exports = {
  content: ["./src/**/*.{html,ts,tsx}"],
  theme: {
    extend: {
      spacing: {
        2.5: "10px",
        15: "3.75rem",
        18: "4.5rem",
      },
      container: {
        center: true,
        padding: "1.25rem",
      },
      fontSize: {
        "2xs": "0.625rem",
      },
      colors: {
        // NSM theme colors
        // TODO: variants for hover, focus ring usw
        "brand-highlight": "var(--brand-highlight, #69A9F8)",
        "brand-highlight-bg": "var(--brand-highlight-bg, #E9F2FD)",
        "brand-stroke": "var(--brand-stroke, #A0CBFF)",
        "brand-bg": "var(--brand-bg, #F7F9FC)",

        "brand-action": "var(--brand-action, #E26E76)",

        "status-confirmed": "var(--status-confirmed, #18CD4B)",
        success: "var(--success, #73C89F)",

        "main-dark": "var(--main-dark, #2B2851)",
        "main-medium": "var(--main-medium, #A6A5B2)",
        "main-subtle": "var(--main-subtle, #D5D4D9)",
        "main-light": "var(--main-light, #F2F1F4)",

        // PREPAY SCHEMA
        "accent-medium-dark": accentColorAbsolute("30%"),
        "accent-darker": accentColorRelative(0.85),
        accent: accentColorAbsolute(),
        "accent-light": accentColorAbsolute("80%"),
        "accent-lighter": accentColorAbsolute("90%"),
        "accent-lightest": accentColorAbsolute("95%"),
      },
      boxShadow: {
        basic:
          "0px 2px 6px rgba(43, 40, 81, 0.04), 0 1px 4px rgba(43, 40, 81, 0.06)",
        hover: "0px 4px 24px rgba(43, 40, 81, 0.15)",
        overlay: "4px 4px 32px rgba(43, 40, 81, 0.25)",
      },
    },
  },
  plugins: [
    require("@tailwindcss/forms"),
    require("@tailwindcss/aspect-ratio"),
  ],
};

function accentColorAbsolute(lightness = "var(--default-accent-color-l)") {
  return ({ opacityValue }) => {
    const baseHSL = `var(--default-accent-color-h) var(--default-accent-color-s) ${lightness}`;
    const baseWithOpacity = `${baseHSL} / ${opacityValue}`;

    return `hsl(${opacityValue === undefined ? baseHSL : baseWithOpacity})`;
  };
}

function accentColorRelative(factor = 1) {
  return ({ opacityValue }) => {
    const baseHSL = `var(--default-accent-color-h) var(--default-accent-color-s) calc(${factor} * var(--default-accent-color-l))`;
    const baseWithOpacity = `${baseHSL} / ${opacityValue}`;

    return `hsl(${opacityValue === undefined ? baseHSL : baseWithOpacity})`;
  };
}
